﻿// Written by Lampros Valais
// Matric number : 40178675
// Ediburgh Napier University 
// Software Development 2 
// Last modified 22 Oct 2015

// Code for Student class in C# to validate data and include the methods award() and advance()
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
namespace Student_Record_System
{
    class Student
    {
        //matriculation number
        private int matric; 
        //first name
        private string first_name;
        //last name
        private string second_name;
        //date of birth, nullable because a user can enter a null value and I prefer to make the null check in my program and not by default
        private DateTime? dateOfBirth;
        //course
        private string course;
        //level 1-4
        private int level;
        //credits 0-480
        private int credits;
        // Bool to check if the data entered in the forms are finally saved in the class.
        private bool exist = false;    
        // Constructor without any arguement
        public Student() { }
        // Constructor with 7 arguements
        public Student(string fn,string ln,DateTime d,string c,int m,int l,int cr)
        {
            First_Name = fn;
            second_name = ln;
            dateOfBirth = d;
            Course = c;
            matric = m;
            level = l;
            credits = cr;
        }
        //public properties
        public int Matric
        {
            get { return matric; }
            set
            {
                //matric validation
                if (value < 40000 || value > 60000)
                {
                    throw new ArgumentException("Out of bounds!Enter a value between 40000 and 60000.");
                }
                matric = value;                
            }
        }   
        public string First_Name
        {
            get { return first_name; }
            set
            {
                //name validation
                if (value == null || value==String.Empty)
                {
                    throw new ArgumentException("First name can't be empty.");
                }
                first_name = value;
            }
        }
        public string Second_Name
        {
            get { return second_name; }
            set
            {
                //name validation
                if (value == null || value == String.Empty)
                {
                    throw new ArgumentException("Last name can't be blank.");
                }
                second_name = value;
            }
        }
        public DateTime? DateOfBirth
        {
            get { return dateOfBirth; }
            set
            {
                // dateofbirth validation - not null and not greater than today's date.
                if (value == null || value>DateTime.Now)
                {
                    throw new ArgumentException("Date of birth can't be blank or greater than today.");
                }
                dateOfBirth = value;
            }
        }
        public string Course
        {
            get { return course; }
            set
            {
                //course validation
                if (value == null || value == String.Empty)
                {
                    throw new ArgumentException("Course can't be blank.");
                }
                course = value;
            }
        }
        public int Level
        {
            get { return level; }
            set
            {
                //level validation
                if (value < 1 || value > 4)
                {
                    throw new ArgumentException("Level should be between 1-4");
                }
                level = value;
            }
        }
        public int Credits
        {
            get { return credits; }
            set
            {
                //credits validation
                if (value < 0 || value > 480 )
                {
                    throw new ArgumentException("Credits must be between 0 - 480");
                }
                credits = value;
            }
        }
        // bool to check if values passed in the class or not.
        public bool Exist
        {
            get { return exist; }
            set { exist = value; }
        }
        //method to advance the student to next level if his credits are enough
        public void advance()
        {
            // Can't advance if already in 4th year
            if (level == 4)
            {
                MessageBox.Show("Student already in 4th year.");
            }
            else if ((level == 1 && credits >= 120)
                  || (level == 2 && credits >= 240)
                  || (level == 3 && credits >= 360))
            {
                level++;
            }
            else
            {
                MessageBox.Show("Insufficient credits!");
            }
        }
        // method to return the award of a student depending his credits.
        public virtual string award()
        {
            if (credits < 359)
            {
                return "Awarded with Certificate of Higher Education.";
            }
            else if (credits > 360 && credits < 479)
            {
               
                return "Awarded with Degree.";
            }
            else 
            {
                return "Awarded with Honours degree.";
            }
        }
    }
}
