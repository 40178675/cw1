﻿// Written by Lampros Valais
// Matric number : 40178675
// Ediburgh Napier University 
// Software Development 2 
// Last modified 20 Oct 2015

// Code for awards window in C# to display the student details and award type
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Student_Record_System
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        // constructor of Window1 recieving 7 arguements 
        public Window1(string first_name,string last_name, int matric, string course, int level, string award )
        {
            InitializeComponent();
            fn_txt.Content = first_name;
            sn_txt.Content = last_name;
            matric_txt.Content = matric;
            course_txt.Content = course;
            level_txt.Content = level;
            award_txt.Content = award;          
        }
        // close button 
        private void close_btn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
