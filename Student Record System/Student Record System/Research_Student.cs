﻿// Written by Lampros Valais
// Matric number : 40178675
// Ediburgh Napier University 
// Software Development 2 
// Last modified 22 Oct 2015

// Code for Research_Student class to validate data and override the method award()

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student_Record_System
{
    // Reseach_Student inherits attributes and methods from Student class.
    class Research_Student:Student
    {
        // private attributes
        private string topic;
        private string supervisor;
        // Constructor
        public Research_Student()
        {
            // course must be always set to phd
            Course = "Phd";
        }
        // get set method for pubic string Topic in order to be accessible from the main window.     
        public string Topic
        {
            get { return topic; }
            set
            {
                // Topic can't be empty
                if (value==null || value==String.Empty)
                {
                    throw new ArgumentException("Topic can't be empty");
                }
                else
                {
                    topic = value;
                }
            }
        }
        public string Supervisor
        {
            get { return supervisor; }
            set
            {
                // Supervisor can't be empty
                if (value==null || value == String.Empty)
                {
                    throw new ArgumentException("Supervisor can't be empty.");
                }
                else
                {
                    supervisor = value;
                }
            }
        }
        // override method to return the award of the research student
        public override string award()
        {
            // will be awarded only if the level is 4
            if (Level == 4)
            {
                return "Doctor of Philosophy";
            }
            else
            {
                return null;
            }
        }
    }
}
