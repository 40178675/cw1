﻿// Written by Lampros Valais
// Matric number : 40178675
// Ediburgh Napier University 
// Software Development 2 
// Last modified 22 Oct 2015

// Code for the main window in C#

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Student_Record_System
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Boolean to check if the research student box is checked, unchecked (false) is the default.
        bool isChecked=false;
        // Boolean to check if there is any data in the fields, true because fields are cleared at start.
        bool isCleared = true;        
        public MainWindow()
        {
            InitializeComponent();
                       
        }
        // Create an instance of student
        Student s = new Student();
        // Create an instance of research student
        Research_Student r = new Research_Student();
        // Set button configuration
        private void set_btn_Click(object sender, RoutedEventArgs e)
        {
           // If research box checked values will be saved to research student class
            if (isChecked)
            {
                // If is checked then the Research_Student r exists
                r.Exist = true;
                try
                {
                    // Fetching data from the forms
                    r.First_Name = fname_txt.Text;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message); // Output the message of error 
                    // if there is an error message then the student doesn't exist
                    r.Exist = false;
                }
                try
                {
                    r.Second_Name = lname_txt.Text;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                    r.Exist = false;
                }
                try
                {
                    r.DateOfBirth = DateTime.Parse(dob_txt.Text).Date;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                    r.Exist = false;
                }
                try
                {
                    r.Course = course_txt.Text;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                    r.Exist = false;
                }
                try
                {
                    // checking if there is any value in the matriculation field
                    if (!string.IsNullOrWhiteSpace(matric_txt.Text))
                    {
                        r.Matric = int.Parse(matric_txt.Text);
                    }
                    else
                    {
                        throw new ArgumentException("Matriculation field can't be blank!");
                    }
                   
                }
                // this will catch the last arguement exception, so it's either the not blank or the validation for the values 40000-60000 from the class
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                    r.Exist = false;
                }
                try
                {
                    if (!string.IsNullOrWhiteSpace(level_txt.Text))
                    {
                        r.Level = int.Parse(level_txt.Text);
                    }
                    else
                    {
                        throw new ArgumentException("Level field can't be blank!");
                    }                    
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                    r.Exist = false;
                }
                try
                {
                    if (!string.IsNullOrWhiteSpace(credits_txt.Text))
                    {
                        r.Credits = int.Parse(credits_txt.Text);
                    }
                    else
                    {
                        throw new ArgumentException("Credits field can't be blank!");
                    }
                    
                }
                catch (Exception excep)
                {
                    MessageBox.Show( excep.Message);
                    r.Exist = false;
                }
                try
                {
                    r.Topic = topic_txt.Text;
                }
                catch (Exception excep)
                {
                    MessageBox.Show( excep.Message);
                    r.Exist = false;
                }
                try
                {
                    r.Supervisor = super_txt.Text;
                }
                catch (Exception excep)
                {
                    MessageBox.Show( excep.Message);
                    r.Exist = false;
                }
                // If exception appears details are not saved
                if (!r.Exist)
                {           
                    MessageBox.Show("Failed to save research student.");
                }
                // Else details are saved and s.Exist can now set to false so we would be able to print the r. details!
                else
                {
                    s.Exist = false;
                    isCleared = false;// data will be in the fields so isCleaned is false.
                    MessageBox.Show("Research student saved.");
                }          
            }
            // If unchecked box
            else
            {
                s.Exist = true;
                // Using the try - catch method to validate each input
                try
                {
                    s.First_Name = fname_txt.Text;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message); // Output the message of error 
                    s.Exist = false; // Will indicate if any exception appears
                }
                try
                {
                    s.Second_Name = lname_txt.Text;
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                    s.Exist = false;
                }
                try
                {
                    s.DateOfBirth = DateTime.Parse(dob_txt.Text).Date;
                }
                catch (Exception excep)
                {
                    MessageBox.Show( excep.Message);
                    s.Exist = false;
                }
                try
                {
                    s.Course = course_txt.Text;
                }
                catch (Exception excep)
                {
                    MessageBox.Show( excep.Message);
                    s.Exist = false;
                }
                try
                {
                    // Validate if it is not a null or blank value. 
                    if (!string.IsNullOrWhiteSpace(matric_txt.Text))
                    {
                        s.Matric = int.Parse(matric_txt.Text);
                    }
                    else
                    {
                        throw new ArgumentException("Matric field can't be blank!");
                    }
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                    s.Exist = false;
                }
                try
                {
                    if (!string.IsNullOrWhiteSpace(level_txt.Text))
                    {
                        s.Level = int.Parse(level_txt.Text);
                    }
                    else
                    {
                        throw new ArgumentException("Level field can't be blank!");
                    }
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                    s.Exist = false;
                }
                try
                {
                    if (!string.IsNullOrWhiteSpace(credits_txt.Text))
                    {
                        s.Credits = int.Parse(credits_txt.Text);
                    }
                    else
                    {
                        throw new ArgumentException("Credits field can't be blank!");
                    }                    
                }
                catch (Exception excep)
                {
                    MessageBox.Show(excep.Message);
                    s.Exist = false;
                }
                // If student details are faulty data will not be saved
                if (!s.Exist)
                {                            
                    MessageBox.Show("Failed to save student.");
                }
                //Else data will be saved and therefore r.Exist can set to false so we will be able to print the s. details
                else
                {
                    isCleared = false;// data will be in the fields so isCleaned is false.
                    r.Exist = false;
                    MessageBox.Show("Student saved.");
                }
            }                    
        }
        // Clear button is doing the job to clean all the fields from the window.
        private void clear_btn_Click(object sender, RoutedEventArgs e)
        {
            fname_txt.Text= String.Empty;
            lname_txt.Text = String.Empty;
            dob_txt.Text = String.Empty;
            course_txt.Text = String.Empty;
            matric_txt.Text = String.Empty;
            level_txt.Text = String.Empty;
            credits_txt.Text = String.Empty;
            topic_txt.Text = String.Empty;
            super_txt.Text = String.Empty;
            //setting the isCleared to true as there will be no data in the fields 
            isCleared = true;
        }
        private void get_btn_Click(object sender, RoutedEventArgs e)
        {
            // Check if previous entry was a student one or not - s.Exist must be True to print.
            if (!isChecked && s.Exist)
            {
                //Display the values of student 
                fname_txt.Text = s.First_Name;
                lname_txt.Text = s.Second_Name;
                dob_txt.Text = s.DateOfBirth.Value.ToShortDateString();
                matric_txt.Text = s.Matric.ToString();
                course_txt.Text = s.Course.ToString();
                level_txt.Text = s.Level.ToString();
                credits_txt.Text = s.Credits.ToString();
                // data are displayed in the forms so isCleared is false
                isCleared = false;
            }
            else if (isChecked && r.Exist)
            {
                // Display the value of research student
                fname_txt.Text = r.First_Name;
                lname_txt.Text = r.Second_Name;
                // using value to get only the date and not the hour
                dob_txt.Text = r.DateOfBirth.Value.ToShortDateString();
                matric_txt.Text = r.Matric.ToString();
                course_txt.Text = r.Course.ToString();
                level_txt.Text = r.Level.ToString();
                credits_txt.Text = r.Credits.ToString();
                topic_txt.Text = r.Topic;  
                super_txt.Text = r.Supervisor;
                isCleared = false;
            }
            else
            {                
                MessageBox.Show("There is no entry for this kind of student.");
            }      
        }
        //Award button 
        private void award_btn_Click(object sender, RoutedEventArgs e)
        {
            // If is research student and there is award display and there is data in the fields(isCleared must be false).
            if (isChecked && r.Exist && !isCleared)
            { 
                if (r.award() != null)
                {
                    // Pass arguments to the new window
                    Window1 Window1 = new Window1(r.First_Name, r.Second_Name, r.Matric, r.Course, r.Level, r.award());
                    Window1.Show();
                }
                else
                {
                    MessageBox.Show("This student can't get an award yet!");
                }
            }
            // If not research student and award exists and isCleared must be false(indicating that data is appearing on screen in the fields) - award for normal students always exist          
            else if (!isChecked && s.Exist && s.award()!= null && !isCleared)
            {
                Window1 Window1 = new Window1(s.First_Name, s.Second_Name, s.Matric, s.Course, s.Level, s.award());
                Window1.Show();                
            }
            // If there is no award            
            else
            {
                MessageBox.Show("There is no entry for any student!");
            }
        }
        // Advance button 
        private void advance_btn_Click(object sender, RoutedEventArgs e)
        {
            // Research student
            if (isChecked && r.Exist && !isCleared)
            {
                // call function from research student class
                r.advance();
                //Display the new level
                level_txt.Text = r.Level.ToString();
            }
            // Just student 
            else if (!isChecked && s.Exist && !isCleared)
            {
                // call function from student class
                s.advance();
                level_txt.Text = s.Level.ToString();
            }
            else
            {
                MessageBox.Show("Entry not found. Try to set some data first.");
            }
        }     
        // Check box unchecked- default or after unchecking it.
        private void checkBox_Unchecked(object sender, RoutedEventArgs e)
        {            
            // isCheck is false
            isChecked = false;
            // Must undo the read only course field 
            course_txt.Text = "";
            course_txt.IsReadOnly = false;
            //Disable visibilities of labels and textboxes(topic and supervisor)
            topic_lbl.Visibility = Visibility.Hidden;
            topic_txt.Visibility = Visibility.Hidden;
            super_lbl.Visibility = Visibility.Hidden;
            super_txt.Visibility = Visibility.Hidden;
        }
        // Checked box
        private void checkBox_Checked(object sender, RoutedEventArgs e)
        {
            // Changing the bool to true if checked
            isChecked = true;
            // Making visible labels and textboxes (topic and supervisor)
            topic_lbl.Visibility = Visibility.Visible;
            topic_txt.Visibility = Visibility.Visible;
            super_lbl.Visibility = Visibility.Visible;
            super_txt.Visibility = Visibility.Visible;
            // Making the course 'php'
            course_txt.Text = "Phd";
            // Preventing the user from typing in the textbox of course
            course_txt.IsReadOnly = true;
        }

    }
}